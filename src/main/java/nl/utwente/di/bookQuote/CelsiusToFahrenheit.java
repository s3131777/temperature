package nl.utwente.di.bookQuote;

public class CelsiusToFahrenheit {
    public double fahrenheitToCelsius(double fahrenheit) {
        return (fahrenheit * 9 / 5) + 32;
    }
}