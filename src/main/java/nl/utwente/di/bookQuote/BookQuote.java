package nl.utwente.di.bookQuote;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

/** Example of a Servlet that gets an ISBN number and returns the book price
 */

public class BookQuote extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private CelsiusToFahrenheit celsiusToFahrenheit;
	
    public void init() throws ServletException {
    	celsiusToFahrenheit = new CelsiusToFahrenheit();
    }

    /**
     *  It takes two parameters: HttpServletRequest for receiving client requests
     *  and HttpServletResponse for sending responses back to the client.
     *  Its output is an HTML page containing a book quote,
     *  including the ISBN number and price obtained from the request parameters.
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Celsius to Fahrenheit";
    
    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Celsius number: " +
                   request.getParameter("Celsius") + "\n" +
                "  <P>Fahrenheit: " +
                   celsiusToFahrenheit.fahrenheitToCelsius(Double.valueOf(request.getParameter("Celsius"))) +
                "</BODY></HTML>");
  }
}
