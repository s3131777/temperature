package nl.utwente.di.bookQuote;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

public class TestQuoter {
    @Test
    public void testBook1 ( ) throws Exception {
        Quoter quoter = new Quoter ( ) ;
        double price = quoter.getBookPrice(Double.valueOf("1")) ;
        Assertions.assertEquals(10.0, price, "Price of book 1");
    }
}
